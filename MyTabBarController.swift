//
//  MyTabBarController.swift
//  DateBook
//
//  Created by SungJaeLEE on 2016. 2. 6..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

class MyTabBarController: UITabBarController
{
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func childViewControllerForStatusBarStyle() -> UIViewController? {
        return nil
    }
}
