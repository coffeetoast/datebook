//
//  Helpers.swift
//  DateBook
//
//  Created by SungJaeLEE on 2016. 2. 5..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import Foundation

let applicationDocumentsDirectory: String = {
   let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)
    return paths[0]
}()