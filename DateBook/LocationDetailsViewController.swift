//
//  LocationDetailsViewController.swift
//  DateBook
//
//  Created by SungJaeLEE on 2016. 1. 28..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

private let dateFormatter: NSDateFormatter = {
    let formatter = NSDateFormatter()
    formatter.dateStyle = .MediumStyle
    formatter.timeStyle = .ShortStyle
    return formatter
}()

class LocationDetailsViewController: UITableViewController
{
    //CoreData......
    var managedObjectContext: NSManagedObjectContext!
    var locationToEdit: Location? {
        didSet {
            if let location = locationToEdit {
                descriptionText = location.locationDescription
                categoryName = location.category
                date = location.date
                coordinate = CLLocationCoordinate2DMake(location.latitude, location.longitude)
                placemark = location.placemark
            }
        }
    }
    var descriptionText = ""
    
    // Outlet & Action......
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var date = NSDate()
    
    @IBAction func done() {
        let hudView = HudView.hudInView(navigationController!.view, animated: true)
        
        let location: Location
        if let temp = locationToEdit {
            hudView.text = "Updated"
            location = temp
        } else {
            hudView.text = "Tagged"
             location = NSEntityDescription.insertNewObjectForEntityForName("Location", inManagedObjectContext: managedObjectContext) as! Location
            location.photoID = nil 
        }
        
       
        location.locationDescription = descriptionTextView.text
        location.category = categoryName
        location.latitude = coordinate.latitude
        location.longitude = coordinate.longitude
        location.date = date
        location.placemark = placemark
        //Image Save...
        if let image = image {
            if !location.hasPhoto {
                location.photoID = Location.nextPhotoID()
            }
            
            if let data = UIImageJPEGRepresentation(image, 0.5) {
                do {
                    try data.writeToFile(location.photoPath, options: .DataWritingAtomic)
                } catch {
                    debugPrint("Error writing file: \(error)")
                }
            }
        }
        
        
        do {
            try managedObjectContext.save()
            afterDelay(0.6) {
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        } catch {
            fatalCoreDataError(error)
        }
        
        
       
        
//        let delayInSeconds = 0.6
//        let when = dispatch_time(DISPATCH_TIME_NOW, Int64(delayInSeconds * Double(NSEC_PER_SEC)))
//        
//        dispatch_after(when, dispatch_get_main_queue()) { () -> Void in
//            self.dismissViewControllerAnimated(true, completion: nil)
//        }
        
        //presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancel() {
        presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //Putting the location info......
    var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    var placemark: CLPlacemark?
    
    // Send CategoryName........
    var categoryName = "No Category"
    
    
    // Showing the image .........
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.layer.cornerRadius = 25
            imageView.layer.masksToBounds = true

        }
    }
    @IBOutlet weak var addPhotoLabel: UILabel!
    var image: UIImage?
    
    func showImage(image: UIImage) {
        imageView.image = image
        imageView.hidden = false
        imageView.frame = CGRect(x: 10, y: 10, width: 260, height: 260)
     
        addPhotoLabel.hidden = true
    }
    
    //listenForBackgroundNotification() .............
    var observer: AnyObject!
    func listenForBackgroundNotification() {
       observer = NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationDidEnterBackgroundNotification, object: nil, queue: NSOperationQueue.mainQueue()) {[weak self] (_) -> Void in
        
        if let strongSelf = self {
            if strongSelf.presentedViewController != nil {
                strongSelf.dismissViewControllerAnimated(false, completion: nil)
            }
            
            strongSelf.descriptionTextView.resignFirstResponder()
        }
        }
    }
    
    // View Life Cycle...........
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = UIColor(red: 255/255.0, green: 250/255.0, blue: 240/255.0, alpha: 1.0)
        
        descriptionTextView.textColor = UIColor(red: 222/255.0, green: 184/255.0, blue: 157/255.0, alpha: 1.0)
        descriptionTextView.backgroundColor = UIColor(red: 255/255.0, green: 250/255.0, blue: 240/255.0, alpha: 1.0)
        
        addPhotoLabel.textColor = UIColor(red: 222/255.0, green: 184/255.0, blue: 157/255.0, alpha: 1.0)
        addPhotoLabel.highlightedTextColor = addPhotoLabel.textColor
        
        addressLabel.textColor = UIColor(red: 222/255.0, green: 184/255.0, blue: 157/255.0, alpha: 1.0)
        addressLabel.highlightedTextColor = addressLabel.textColor
        
        listenForBackgroundNotification()
        
        if let location = locationToEdit {
            title = "Edit Location"
            if location.hasPhoto {
                if let image = location.photoImage {
                    showImage(image)
                }
            }
        }
        
        updateUI()
        addGesture()
        
        descriptionTextView.text = descriptionText
        
        

    }
    
    deinit {
        print("*** deinit \(self)")
        NSNotificationCenter.defaultCenter().removeObserver(observer)
    }
    
    func updateUI() {
        descriptionTextView.text = ""
        categoryLabel.text = categoryName
        
        latitudeLabel.text = String(format: "%.8f", coordinate.latitude)
        longitudeLabel.text = String(format: "%.8f", coordinate.longitude)
        
        if let placemark = placemark {
            addressLabel.text = stringFromPlacemark(placemark)
        } else {
            addressLabel.text = "No Address Found"
        }
        
        dateLabel.text = formatDate(date)
    }
    
    func addGesture() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard:"))
        gestureRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(gestureRecognizer)
    }
    
    func hideKeyboard(gestureRecognizer: UIGestureRecognizer) {
//        let point = gestureRecognizer.locationInView(tableView)
//        let indexPath = tableView.indexPathForRowAtPoint(point)
//        
//        if indexPath != nil && indexPath!.section == 0
//            && indexPath!.row == 0 {
//                return
//        }
        descriptionTextView.resignFirstResponder()
    }
   
    //PrepareForSegue................
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PickCategory" {
            let controller = segue.destinationViewController as! CategoryPickerViewController
            controller.selectedCategoryName = categoryName
        }
    }
}

//MARK: - Putting the location info......
extension LocationDetailsViewController
{
    func stringFromPlacemark(placemark: CLPlacemark) -> String {
      var line = ""
        line.addText(placemark.subThoroughfare)
        line.addText(placemark.thoroughfare, withSeparator: " ")
        line.addText(placemark.locality, withSeparator: ", ")
        line.addText(placemark.administrativeArea, withSeparator: ", ")
        line.addText(placemark.postalCode, withSeparator: " ")
        line.addText(placemark.country, withSeparator: ", ")
        return line
    }
    
    func formatDate(date: NSDate) -> String {
        return dateFormatter.stringFromDate(date)
    }
}

//MARK: - UITableViewDelegate..........
extension LocationDetailsViewController
{
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor(red: 255/255.0, green: 250/255.0, blue: 240/255.0, alpha: 1.0)
        
        if let textLabel = cell.textLabel {
            textLabel.textColor = UIColor(red: 222/255.0, green: 184/255.0, blue: 157/255.0, alpha: 1.0)
            textLabel.highlightedTextColor = textLabel.textColor
        }
        
        if let detailLabel = cell.detailTextLabel {
            detailLabel.textColor = UIColor(red: 222/255.0, green: 184/255.0, blue: 157/255.0, alpha: 1.0)
            detailLabel.highlightedTextColor = detailLabel.textColor
        }
        
        let selectionView = UIView(frame: CGRect.zero)
        selectionView.backgroundColor = UIColor(red: 255/255.0, green: 250/255.0, blue: 240/255.0, alpha: 0.2)
        cell.selectedBackgroundView = selectionView
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        if indexPath.section == 0 || indexPath.section == 1 {
            return indexPath
        } else {
            return nil
        }
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return 88
        } else if indexPath.section == 1 {
            if imageView.hidden {
                return 44
            } else {
                return 280
            }
            
            
        } else if indexPath.section == 2 && indexPath.row == 2 {
            addressLabel.frame.size = CGSize(width: view.bounds.size.width - 115, height: 10000)
            addressLabel.sizeToFit()
            addressLabel.frame.origin.x = view.bounds.size.width - addressLabel.frame.size.width - 15
            return addressLabel.frame.size.height + 20
            
        } else {
            return 44
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            descriptionTextView.becomeFirstResponder()
        }
        
        else if indexPath.section == 1 && indexPath.row == 0 {
            pickPhoto()
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
}

//MARK: - Unwind Segue.................
extension LocationDetailsViewController
{
    @IBAction func categoryPickerDidPickCategory(segue: UIStoryboardSegue){
        let controller = segue.sourceViewController as! CategoryPickerViewController
        categoryName = controller.selectedCategoryName
        categoryLabel.text = categoryName
    }
}

//MARK: - UIImagePickerControllerDelegate..............
extension LocationDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func pickPhoto() {
        if true || UIImagePickerController.isSourceTypeAvailable(.Camera) {
            showPhotoMenu()
        } else {
            choosePhotoFromLibrary()
        }
    }

    func showPhotoMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: .Default, handler: { _ in self.takePhotoWithCamera() })
        alertController.addAction(takePhotoAction)
        
        let chooseFromLibraryAction = UIAlertAction(title: "Choose From Library", style: .Default, handler: { _ in self.choosePhotoFromLibrary() })
        alertController.addAction(chooseFromLibraryAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
 
    func takePhotoWithCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.view.tintColor = view.tintColor
        imagePicker.sourceType = .Camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        presentViewController(imagePicker, animated: true, completion: nil)
    }
   
    func choosePhotoFromLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.view.tintColor = view.tintColor
        imagePicker.sourceType = .PhotoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        image = info[UIImagePickerControllerEditedImage] as? UIImage
        if let image = image {
            showImage(image)
         
        }
        
        tableView.reloadData()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
}