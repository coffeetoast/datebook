//
//  String+AddText.swift
//  DateBook
//
//  Created by SungJaeLEE on 2016. 2. 6..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import Foundation

extension String
{
    mutating func addText(text: String?, withSeparator separator: String = "") {
        if let text = text {
            if !isEmpty {
                self += separator
            }
            self += text
        }
    }
}