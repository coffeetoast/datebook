//
//  CategoryPickerViewController.swift
//  DateBook
//
//  Created by SungJaeLEE on 2016. 1. 28..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

class CategoryPickerViewController: UITableViewController
{
    // Data Model.....
    var selectedCategoryName = ""
    let categories = [
        "No Category",
        "LandScape",
        "Coffee",
        "Restaurant",
        "LandMark",
        "Park",
        "Museum",
        "Bizarre Place",
        "In Travel"
    ]
    
    var selectedIndexPath = NSIndexPath()
    
    // View Life Cycle..........
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = UIColor(red: 255/255.0, green: 250/255.0, blue: 240/255.0, alpha: 1.0)
        
        
        for i in 0..<categories.count {
            if categories[i] == selectedCategoryName {
                selectedIndexPath = NSIndexPath(forRow: i, inSection: 0)
                break
            }
        }
    }
    
    //PrepareForSegue................
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PickedCategory" {
            let cell = sender as! UITableViewCell
            if let indexPath = tableView.indexPathForCell(cell) {
                selectedCategoryName = categories[indexPath.row]
            }
        }
    }

}

//MARK: - UITableViewDataSource...........
extension CategoryPickerViewController
{
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        let categoryName = categories[indexPath.row]
        cell.textLabel!.text = categoryName
        
        if categoryName == selectedCategoryName {
            cell.accessoryType = .Checkmark
        } else {
            cell.accessoryType = .None
        }
        
        return cell
    }
}

//MARK: - UITableViewDelegate............
extension CategoryPickerViewController
{
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row != selectedIndexPath.row {
            if let newCell = tableView.cellForRowAtIndexPath(indexPath) {
                newCell.accessoryType = .Checkmark
            }
            
            if let oldCell = tableView.cellForRowAtIndexPath(selectedIndexPath) {
                oldCell.accessoryType = .None
            }
            
            selectedIndexPath = indexPath
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor(red: 255/255.0, green: 250/255.0, blue: 240/255.0, alpha: 1.0)
        
        if let textLabel = cell.textLabel {
            textLabel.textColor = UIColor(red: 222/255.0, green: 184/255.0, blue: 157/255.0, alpha: 1.0)
            textLabel.highlightedTextColor = textLabel.textColor
        }
        
        let selectionView = UIView(frame: CGRect.zero)
        selectionView.backgroundColor = UIColor(red: 255/255.0, green: 250/255.0, blue: 240/255.0, alpha: 0.2)
        cell.selectedBackgroundView = selectionView
    }
}