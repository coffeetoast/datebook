//
//  Helper.swift
//  DateBook
//
//  Created by SungJaeLEE on 2016. 1. 29..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import Foundation


func afterDelay(seconds: Double, closure: () -> ())
{
    let when = dispatch_time(DISPATCH_TIME_NOW,Int64(seconds * Double(NSEC_PER_SEC)))
    dispatch_after(when, dispatch_get_main_queue(), closure)
}